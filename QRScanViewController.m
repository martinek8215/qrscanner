//
//  QRScanViewController.m
//  Enmasses
//
//  Created by Adrian on 26/04/15.
//
//

#import "QRScanViewController.h"
#import "ZBarImageScanner.h"

#define REQUEST_QRCODE_INFO 1

@interface QRScanViewController ()

@end

@implementation QRScanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UILabel * titleView = [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width-100)/2, 0, 100, 32)];
    titleView.text = [Utils localizedText:@"em_qr_scanner"];
    titleView.textAlignment = NSTextAlignmentCenter;
    titleView.textColor = [UIColor whiteColor];
    titleView.backgroundColor = [UIColor clearColor];
    titleView.font = [UIFont boldSystemFontOfSize:17.0];
    [titleView sizeToFit];
    self.navigationItem.titleView = titleView;
    [titleView release];

    [self performSelector:@selector(init_camera) withObject:nil afterDelay:1.0f];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidAppear:(BOOL)animated
{
    for (UIImageView *i in self.view.subviews) {
        if ([i isKindOfClass:[UIImageView class]]) {
            [i removeFromSuperview];
        }
    }
    [self performSelector:@selector(start_camera) withObject:nil afterDelay:0.5f];
}

- (void) start_camera
{
    if( isCamera )
        [reader start];
}

- (void) init_camera
{
    reader = [ZBarReaderView new];
    ZBarImageScanner * scanner = [ZBarImageScanner new];
    [scanner setSymbology:ZBAR_PARTIAL config:0 to:0];
    [reader initWithImageScanner:scanner];
    [scanner release];
    reader.readerDelegate = self;
    
    h = [UIScreen mainScreen].bounds.size.height - self.tabBarController.tabBar.frame.size.height - self.navigationController.navigationBar.frame.size.height;
    w = [UIScreen mainScreen].bounds.size.width;
    const float h_padding = w / 10.0;
    const float v_padding = h / 10.0;
    x = h_padding;
    y = v_padding;
    w = w - h_padding * 2;
    h = h - v_padding * 2;
    CGRect reader_rect = CGRectMake(x, y, w, h );
    reader.frame = reader_rect;
    reader.backgroundColor = [UIColor redColor];
    [reader start];
    isCamera = true;
    
    [self.view addSubview: reader];
    [reader release];
}

- (void) readerView:(ZBarReaderView *)readerView didReadSymbols: (ZBarSymbolSet *)symbols fromImage:(UIImage *)image
{
    ZBarSymbol * s = nil;
    for (s in symbols)
    {
        NSLog(@"%@", s.data);
        //        text.text = s.data;
        //        image_view.image = image;
        self.strQRCode =  s.data;
       
        didDecodeImage = image;
        
        // into my ViewController: 450
        imgView =[[UIImageView alloc] initWithImage:didDecodeImage];
        
        imgView.frame = CGRectMake(x, y, w, h);
        [self.view addSubview:imgView];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        
        // getting an NSString
        NSString* domain = [prefs stringForKey:@"domain"];
        NSString* token = [prefs stringForKey:@"token"];
        
        NSString* postData = [NSString stringWithFormat:@"qr_code=%@&token=%@",self.strQRCode,token];
        
        NSString* strUrl = [NSString stringWithFormat:@"%@%@",domain,URI_COUPON_DETAIL];
        
        DLog(@"QrCodeScannerViewControler : %@",strUrl);
        DLog(@"QrCodeScannerViewControler : %@",postData);
        prefs = NULL;
        
        // Ket noi thuc hien check login
        connection = [[GrabRequestHttp alloc] initWithDelegate:self andMessageId:REQUEST_QRCODE_INFO];
        
        [connection sentPostRequest:strUrl
                         withParams:postData];
    }
}

#pragma mark -
#pragma mark overwrite Connection

-(void) onRequestCompleteWithContent:(NSString *)content andMessageId:(int)messageId {
    
    [imgView removeFromSuperview];
    [reader stop];
    
    if(messageId == REQUEST_QRCODE_INFO) {
        
        NSMutableDictionary* data = (NSMutableDictionary*) content;
        
        [data setObject:didDecodeImage forKey:@"didDecodeImage"];
        
        [appDelegate displayScreen:QR_INFO_CONTROLLER withParams:data withNvaController:[self navigationController]];
        NSLog(@"displayScreen sent");
        return;
    }
    
    [super onRequestCompleteWithContent:content andMessageId:messageId];
    
}

-(void) onRequestErrorWithContent:(NSString *)content andMessageId:(int)messageId {
    
    [imgView removeFromSuperview];
    [reader stop];
    
    if(messageId == REQUEST_QRCODE_INFO) {
        
        NSMutableDictionary* data = [[NSMutableDictionary alloc] init] ;
        
        [data setObject:self.strQRCode forKey:@"coupon_code"];
        [data setObject:didDecodeImage forKey:@"didDecodeImage"];
        
        [appDelegate displayScreen:QR_INVALID_INFO_CONTROLLER withParams:data withNvaController:[self navigationController]];
        return;
    }
    
    [super onRequestErrorWithContent:content andMessageId:messageId];
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)buttonPressed:(id)sender {
    [reader start];
}
@end
